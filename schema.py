# flask_sqlalchemy/schema.py
import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from models import db_session, Department as DepartmentModel, Employee as EmployeeModel


class Department(SQLAlchemyObjectType):
    class Meta:
        model = DepartmentModel
        interfaces = (relay.Node, )


class DepartmentConnection(relay.Connection):
    class Meta:
        node = Department


class Employee(SQLAlchemyObjectType):
    class Meta:
        model = EmployeeModel
        interfaces = (relay.Node, )


class EmployeeConnections(relay.Connection):
    class Meta:
        node = Employee

    total_count = graphene.Int()

    def resolve_total_count(self, info, **args):
        return self.length

class EmployeePage(graphene.ObjectType):
    page_size = graphene.Int()
    employee_page = graphene.List(Employee)
    total_pages = graphene.Int()
    total_length = graphene.Int()

    def resolve_total_pages (self, info, **args):
        num = self.total_length / self.page_size
        r = self.total_length % self.page_size

        return num if r == 0 else num + 1

class Query(graphene.ObjectType):
    node = relay.Node.Field()
    employee_page_query = graphene.Field(EmployeePage, psize=graphene.Int(), pnum=graphene.Int())
    # Allows sorting over multiple columns, by default over the primary key
    all_employees = SQLAlchemyConnectionField(EmployeeConnections)
    # Disable sorting over this field
    all_departments = SQLAlchemyConnectionField(DepartmentConnection, sort=None)

    def resolve_employee_page_query (self, info, **args):
        page_size = args.get('psize')
        page_offset = args.get('pnum') - 1
        curr_page_offset = page_size*page_offset

        buf = Employee.get_query(info).order_by("department_id")

        employee_page = buf.slice(curr_page_offset, curr_page_offset + page_size)

        #perhaps a little inefficient, with the multiple calls
        return EmployeePage(page_size=page_size, employee_page=employee_page, total_length=len(buf.all()))

schema = graphene.Schema(query=Query)